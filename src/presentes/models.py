from django.db import models

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token 

# Create your models here.
class Base(models.Model):
    criacao = models.DateTimeField(auto_now_add=True)
    atualizacao = models.DateTimeField(auto_now=True)
    ativo = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Presentes(Base):
    id = models.AutoField(primary_key=True)
    titulo = models.CharField(max_length=255)
    descricao = models.CharField(max_length=255, default='')
    url = models.URLField(unique=True, default='')
    foto = models.ImageField(upload_to='presentes', blank=True)

    class Meta:
        verbose_name = 'Presente'
        verbose_name_plural = 'Presentes'
        unique_together = ['titulo']
        ordering = ['id']

    def __str__(self):
        return self.titulo
