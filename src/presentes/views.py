from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
import random 

from .models import Presentes
from .serializers import PresentesSerializer


class PresentesViewSet(viewsets.ModelViewSet):
    queryset = Presentes.objects.all()
    serializer_class = PresentesSerializer

class PresenteRandomViewSet(APIView):
    
    def get(self, request,):
        queryset = Presentes.objects.all()
        id_random = random.choice(queryset)
        serializer = PresentesSerializer(id_random)
        return Response(serializer.data)