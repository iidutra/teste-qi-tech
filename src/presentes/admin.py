from django.contrib import admin

from .models import Presentes


@admin.register(Presentes)
class PresentesAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'descricao', 'url', 'foto', 'criacao', 'atualizacao', 'ativo')