from django.urls import path

from rest_framework.routers import SimpleRouter

from .views import PresentesViewSet, PresenteRandomViewSet

router = SimpleRouter()
router.register('presentes', PresentesViewSet)

urlpatterns = [
    path('presente/', PresenteRandomViewSet.as_view(), name='presente'),
]