from rest_framework import serializers
from .models import Presentes

class PresentesSerializer(serializers.ModelSerializer):


    class Meta:
        model = Presentes
        fields = (
            'id',
            'titulo',
            'descricao',
            'url',
            'foto',
            'criacao',
            'ativo'
        )