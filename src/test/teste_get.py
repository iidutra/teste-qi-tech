import requests

url_base_lista = 'http://localhost:8000/api/v1/presentes/'

resultado = requests.get(url_base_lista)
# print(resultado.json())

# testando se o endpoint esta correto
assert resultado.status_code == 200

# testando a quantidade de registros
assert resultado.json()['count'] == 3

# testando se titulo da 3 presente esta correto
assert resultado.json()['results'][1]['titulo'] == 'Monitor Gamer'