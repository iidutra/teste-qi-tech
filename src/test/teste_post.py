import requests

url_base_lista = 'http://localhost:8000/api/v1/presentes/'



novo_presente = {
    "titulo": "Jogo God Of War 4",
    "descricao": "Jogo da hora",
    "url": "http://psstore.com",
}

resultado = requests.post(url=url_base_lista, data=novo_presente)

# testando o codigo de status http 201
assert resultado.status_code == 201

# testando se o titulo do presente é o mesmo do informado
assert resultado.json()['titulo'] == novo_presente['titulo']

