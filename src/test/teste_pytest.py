import requests

class TestPresentes:
    url_base_lista = 'http://localhost:8000/api/v1/presentes/'
    headers = {'Authorization': 'Token 13127d997eb33006b06b898279869cd2220daad1'}

    def test_get_presentes(self):
        resposta = requests.get(url=self.url_base_lista)

        assert resposta.status_code == 200
    
    def test_get_presente(self):
        resposta = requests.get(url=f'{self.url_base_lista}1/')

        assert resposta.status_code == 200

    def test_post_presente(self):
        novo = {
            "titulo": "Celular",
            "url": "https://saraiva.com.br"
        }
        resposta = requests.post(url=self.url_base_lista, data=novo)
        
        assert resposta.status_code == 201
        assert resposta.json()['titulo'] == novo['titulo']
    
    def test_put_presente(self):
        atualizado = {
            "titulo": "Livro Dos Insultos",
            "url": "http://saraiva.com.br"
        }

        resposta = requests.put(url=f'{self.url_base_lista}0/', data=atualizado)

        assert resposta.status_code == 200
    
    def test_put_titulo_presente(self):
        atualizado = {
            "titulo": "Novo Livro atualizado"
        }
        resposta = requests.put(url=f'{self.url_base_lista}0/', data=atualizado)

        assert resposta.json()['titulo'] == atualizado['titulo']

    def test_delete_curso(self):
        resposta = requests.delete(url=f'{self.url_base_lista}0/')

        assert resposta.status_code == 204 and len(resposta.text) == 0