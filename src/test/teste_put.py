import requests

url_base_lista = 'http://localhost:8000/api/v1/presentes/'


presente_atualizado = {
    "titulo": "Monitor Gamer LG",
    "url": "http://mercadolivre.com"
}

# buscando o presente com id 2
presente = requests.get(f'{url_base_lista}1/')

resultado = requests.put(url=f'{url_base_lista}1/', data=presente_atualizado)

# testando o codigo de status http
assert resultado.status_code == 200

# testando o titulo
assert resultado.json()['titulo'] == presente_atualizado['titulo']